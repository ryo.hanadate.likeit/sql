  INSERT INTO item VALUES(1,'堅牢な机',3000,1);
  INSERT INTO item VALUES(2,'生焼け肉',50,2);
  INSERT INTO item VALUES(3,'すっきりわかるJava入門',3000,3);
  INSERT INTO item VALUES(4,'おしゃれな椅子',2000,4);
  INSERT INTO item VALUES(5,'こんがり肉',500,2);
  INSERT INTO item VALUES(6,'書き方ドリルSQL',2500,3);
  INSERT INTO item VALUES(7,'ふわふわのベッド',30000,1);
  INSERT INTO item VALUES(8,'ミラノ風ドリア',300,2);

  UPDATE item SET category_id = 1 WHERE item_id = 4;

  SELECT * FROM item;
+---------+---------------------------------+------------+-------------+
| item_id | item_name                       | item_price | category_id |
+---------+---------------------------------+------------+-------------+
|       1 | 堅牢な机                        |       3000 |           1 |
|       2 | 生焼け肉                        |         50 |           2 |
|       3 | すっきりわかるJava入門          |       3000 |           3 |
|       4 | おしゃれな椅子                  |       2000 |           1 |
|       5 | こんがり肉                      |        500 |           2 |
|       6 | 書き方ドリルSQL                 |       2500 |           3 |
|       7 | ふわふわのベッド                |      30000 |           1 |
|       8 | ミラノ風ドリア                  |        300 |           2 |
+---------+---------------------------------+------------+-------------+
