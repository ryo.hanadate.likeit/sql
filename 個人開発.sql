CREATE TABLE user(
     id int PRIMARY KEY AUTO_INCREMENT,
     login_id varchar(100) UNIQUE NOT NULL,
     password varchar(100) NOT NULL,
     name varchar(100) UNIQUE NOT NULL,
     birth_date date NOT NULL,
     create_date date NOT NULL,
     update_date date NOT NULL);


CREATE TABLE shift_schedule(
     shift_schedule_id int PRIMARY KEY AUTO_INCREMENT,
     date date UNIQUE NOT NULL,
     work_start int NOT NULL,
     work_end int NOT NULL);


CREATE TABLE work_schedle(
     work_schedele_id int PRIMARY KEY AUTO_INCREMENT,
     req_date date NOT NULL,
     lesson_number int UNIQUE NOT NULL,
     req_teacher int NOT NULL);


CREATE TABLE shift_management(
     shift_management_id int PRIMARY KEY AUTO_INCREMENT,
     date date NOT NULL,
     lesson_number int NOT NULL,
     name varchar(100) NOT NULL);
