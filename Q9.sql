SELECT
  category_name,
  SUM(item_price) AS total_price
FROM
  item
LEFT OUTER JOIN
  item_category
ON
  item.category_id = item_category.category_id
GROUP BY
  item_category.category_name
ORDER BY
  total_price ASC
;
